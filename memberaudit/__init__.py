""""An Alliance Auth app that provides full access to Eve characters
and related reports for auditing, vetting and monitoring.
"""

default_app_config = "memberaudit.apps.MemberAuditConfig"

__version__ = "2.9.2"
__title__ = "Member Audit"
